import { Schema, model } from "mongoose";
import elasticsearch from "@elastic/elasticsearch";
import { ElasticClient } from "./elastic";
const mongoosastic = require("mongoosastic");
export const ItemSchema = new Schema({
  name: {
    type: String,
    es_indexed: true,
  },
});

const esClient: elasticsearch.Client = ElasticClient.getClient();
ItemSchema.plugin(mongoosastic, {
  esClient,
});

export const Item = model("Item", ItemSchema);
