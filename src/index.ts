import express from "express";
import mongoose from "mongoose";
import { ElasticClient } from "./elastic";
import { Item, ItemSchema } from "./Item.model";

const main = async () => {
  const port = process.env.PORT ?? 3000;
  const app = express();
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));

  console.log("CONNECTING TO DB");
  await mongoose
    .connect("mongodb://mongodb:27017", {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then(async () => {
      console.log("DB Created");
      const item = new Item({ name: "Item 0" });
      await item.save();
      console.log("ITEM CREATED");
      console.log("item", item);
    })
    .catch((e) => {
      console.log("Error connecting to the database");
      console.log("e", e);
    });

  // @ts-ignore
  Item.createMapping((err, mapping) => {
    if (err) {
      console.log("Error create mapping");
      console.log(err);
    } else {
      console.log("Mapping Completed");
      console.log("mapping", mapping);
    }
  });

  // @ts-ignore
  const stream = Item.synchronize();
  let count = 0;

  stream.on("data", () => {
    count++;
  });

  stream.on("close", () => {
    console.log(`streamed ${count} items`);
  });

  stream.on("error", (error) => {
    console.log("error", error);
  });

  app.get("/items", async (req, res, next) => {
    if (req.query.search) {
      // @ts-ignore
      Item.search(
        {
          query_string: { query: req.query.search },
        },
        (err, items) => {
          if (err) return next(err);
          const data = items.hits.hits.map((hit) => {
            return hit;
          });

          res.json({ data, search: req.query.search });
        }
      );
    }
  });

  app.post("/items", async (req, res) => {
    const item = new Item({ name: req.body.name });
    await item.save();
    res.json({ item });
  });

  app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
  });
};

main();
