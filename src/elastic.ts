import elasticsearch from "@elastic/elasticsearch";

type MapField = Record<string, string>;
interface CreateMapArgs {
  fields: MapField;
  index: string;
  type: string;
}
interface MappedSchema {
  [key: string]: {
    type: string;
  };
}

export class ElasticClient {
  constructor() {}
  private static elasticUrl =
    `http://elasticsearch:9200/` ?? `http://localhost:9200/`;
  private static _elasticClient: elasticsearch.Client;

  public static getClient() {
    console.log("this.elasticUrl", this.elasticUrl);
    if (!this._elasticClient) {
      this._elasticClient = new elasticsearch.Client({
        node: this.elasticUrl,
      });
    }

    return this._elasticClient;
  }

  public static async createIndex(index: string) {
    try {
      await this.getClient().indices.create({ index });
      console.log(`Created index ${index}`);
    } catch (error) {
      console.error(`An error occurred while creating the index ${index}:`);
      console.error(error);
    }
  }

  public static async mapSchema(args: CreateMapArgs) {
    try {
      const schema = Object.keys(args.fields).reduce((mappedSchema, key) => {
        mappedSchema[key] = {
          type: args.fields[key],
        };

        return mappedSchema;
      }, {} as MappedSchema);

      console.log("schema", schema);

      await this.getClient().indices.putMapping({
        index: args.index,
        type: args.type,
        // include_type_name: true,
        body: {
          properties: schema,
        },
      });

      console.log("Quotes mapping created successfully");
    } catch (error) {
      console.error("An error occurred while setting the quotes mapping:");
      console.error(JSON.stringify(error, undefined, 2));
    }
  }
}
